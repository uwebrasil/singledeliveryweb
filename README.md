# SingleDeliveryWeb

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

## Some links and snippets

### Create ASP.Net core VUE Template manually
https://www.dotnetcurry.com/aspnet-core/1500/aspnet-core-vuejs-template

### Use existing Template (versão de npm precisa ser bem recente)
https://marketplace.visualstudio.com/items?itemName=alexandredotnet.vuejsdotnetfive

### Test Login in PowerShell
```
$Body = @{
    Login = ""
    Password = ""
}
 
$Parameters = @{
    Method = "POST"
    Uri =  "https://localhost:44336/api/token/tokensuperuser"
    Body = ($Body | ConvertTo-Json) 
    ContentType = "application/json"
}
Invoke-RestMethod @Parameters
```