import Vue from "vue";

import VueGoodTablePlugin from "vue-good-table";
import "vue-good-table/dist/vue-good-table.css";
Vue.use(VueGoodTablePlugin);

import VueConfirmDialog from "vue-confirm-dialog";
Vue.use(VueConfirmDialog);
Vue.component("vue-confirm-dialog", VueConfirmDialog.default);

import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";
const options = {
  position: "bottom-right",
};
Vue.use(Toast, options);

import {
  localize,
  ValidationObserver,
  ValidationProvider,
  extend,
} from "vee-validate";
import * as rules from "vee-validate/dist/rules";
Vue.component("ValidationObserver", ValidationObserver);
Vue.component("ValidationProvider", ValidationProvider);
import en from "vee-validate/dist/locale/en.json";
import pt_BR from "vee-validate/dist/locale/pt_BR.json";
localize({
  en,
  pt_BR,
});
localize("pt_BR");
// install rules
Object.keys(rules).forEach((rule) => {
  extend(rule, rules[rule]);
});

import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

import App from "./App.vue";

import Home from "@/components/Home";
import LogMeIn from "@/components/LogMeIn";
import ApiTester from "@/components/ApiTester";
import MainPage from "@/components/MainPage";
import MobileMainPage from "@/components/_mobileMainPage";
import MobilePrecadastrados from "@/components/_mobilePrecadastrados";
import MobileFormulario from "@/components/_mobileFormulario";
import MobileSearch2 from "@/components/_mobileSearch2";

import VueRouter from "vue-router";
Vue.use(VueRouter);

Vue.config.productionTip = false;

const router = new VueRouter({
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home,
    },
    {
      path: "/logmein",
      name: "LogMeIn",
      component: LogMeIn,
    },
    {
      path: "/mainpage",
      name: "MainPage",
      component: MainPage,
    },
    {
      path: "/apitester",
      name: "ApiTester",
      component: ApiTester,
    },
    {
      path: "/mmainpage",
      name: "_mobileMainPage",
      component: MobileMainPage,
    },
    {
      path: "/mprecadastrados",
      name: "_mobilePrecadastrados",
      component: MobilePrecadastrados,
    },
    {
      path: "/mformulario",
      name: "_mobileFormulario",
      component: MobileFormulario,
    },
    {
      path: "/msearch2",
      name: "_mobileSearch2",
      component: MobileSearch2,
    },
  ],
});

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
