var address = new Object();
address.id = 0;
address.state = "";
address.city = "";
address.neighborhood = "";
address.streetName = "";
address.streetNumber = "";
address.postalCode = "";
address.longitude = 0;
address.latitude = 0;

var merchant = new Object();
merchant.id = 0;
merchant.address = address;
merchant.name = "";
merchant.cnpj = "";
merchant.email = "";

// not in form ////////
merchant.city = "";
merchant.imageId = "";
merchant.bannerId = "";
///////////////////////

merchant.merchantStatus = 1;
merchant.status = 0;
merchant.category = 0;

// not in form /////////////
merchant.instagram = "";
merchant.whatsapp = "";
merchant.rating = 0.0;
merchant.deliveryFee = 0.0;
merchant.deliveryFeeFree = 0.0;
merchant.minimumOrder = 0.0;
////////////////////////////

merchant.commission = 5;
merchant.phone = "";
merchant.password = "";
merchant.manager = "";

export default merchant;
